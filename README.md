# transpride-flag-bashrc-addition
A trans pride flag for your Bash Terminal, to be placed to launch from your bashrc to greet you every Terminal opening. See README and LICENSE.

**NOTE, THE COLORS OF THE FLAG MAY VARY DEPENDING ON YOUR TERMINAL EMULATOR, VARIOUS OTHER SETTINGS, AND HOW IT DECIDES TO INTERPRET COLORS- YOUR TERMINAL SHOULD HAVE SETTINGS FOR ADJUSTING THE COLORS TO BE ACCURATE IN A MENU OR CONFIG FILE DEPENDING ON TERMINAL EMULATOR.**

# Usage
- Download the file `transpride.sh` from the repo, and put it somewhere it can stay permanently. I choose to place mine in `/opt/`.
- Open your .bashrc file for editing with the following command: `nano ~/.bashrc`
- Scroll down to the bottom of your bashrc in the editor with the arrow keys, Page Down key and END key.
- At the bottom, DEPENDING ON WHERE YOU PLACED THE SCRIPT, write something like this to reference it: `bash /opt/transpride.sh`.
- After writing that, press `Ctrl`+`X`, type `y` then hit `Enter`.
- `nano` should now be closed, and the process is complete. Open a new Terminal, and you will see the flag!

If you have any troubles using this script, please contact me at #zoeyglobe at irc.freenode.net, or via email to ZoeyGlobe@gmail.com - and I will help you.

# Screenshot
Here is how it looks in my Terminal [Terminator] (with the proper colors configured, but it looks okay by default too);

![screenshot.png](screenshot.png)

Stay prideful in what and who you are, you are loved.

